package sportgrounds.model;

import java.util.Objects;

public class ProfileModel {
    private long profile_id;
    private String login;
    private String password;
    private String link;
    private String nickname;
    private String rating;
    private long contracts;

    public ProfileModel(int profile_id, String login, String password, String link, String nickname, String rating, int contracts) {
        this.profile_id = profile_id;
        this.login = login;
        this.password = password;
        this.link = link;
        this.nickname = nickname;
        this.rating = rating;
        this.contracts = contracts;
    }

    public long getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(long profile_id) {
        this.profile_id = profile_id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public long getContracts() {
        return contracts;
    }

    public void setContracts(long contracts) {
        this.contracts = contracts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileModel that = (ProfileModel) o;
        return profile_id == that.profile_id &&
                contracts == that.contracts &&
                login.equals(that.login) &&
                password.equals(that.password) &&
                link.equals(that.link) &&
                nickname.equals(that.nickname) &&
                rating.equals(that.rating);
    }

    @Override
    public String toString() {
        return "ProfileModel{" +
                "profile_id=" + profile_id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", link='" + link + '\'' +
                ", nickname='" + nickname + '\'' +
                ", rating='" + rating + '\'' +
                ", contracts=" + contracts +
                '}';
    }
}
