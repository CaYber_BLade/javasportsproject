package sportgrounds.model;

import java.util.Objects;

public class SportGroundModel {
    private long ground_id;
    private String country;
    private String city;
    private String street;
    private String home;
    private String worktime;
    private long profile;
    private long owner_id;

    public SportGroundModel() {
    }

    public SportGroundModel(int ground_id, String country, String city, String street, String home, String worktime, int profile, int owner_id) {
        this.ground_id = ground_id;
        this.country = country;
        this.city = city;
        this.street = street;
        this.home = home;
        this.worktime = worktime;
        this.profile = profile;
        this.owner_id = owner_id;
    }

    public long getGround_id() {
        return ground_id;
    }

    public void setGround_id(long ground_id) {
        this.ground_id = ground_id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getWorktime() {
        return worktime;
    }

    public void setWorktime(String worktime) {
        this.worktime = worktime;
    }

    public long getProfile() {
        return profile;
    }

    public void setProfile(long profile) {
        this.profile = profile;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SportGroundModel that = (SportGroundModel) o;
        return ground_id == that.ground_id &&
                profile == that.profile &&
                owner_id == that.owner_id &&
                country.equals(that.country) &&
                city.equals(that.city) &&
                street.equals(that.street) &&
                home.equals(that.home) &&
                worktime.equals(that.worktime);
    }

    @Override
    public String toString() {
        return "SportGroundModel{" +
                "ground_id=" + ground_id +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", home='" + home + '\'' +
                ", worktime='" + worktime + '\'' +
                ", profile=" + profile +
                ", owner_id=" + owner_id +
                '}';
    }
}
