package sportgrounds.model;

import java.util.Objects;

public class OwnerModel {
    private long id;
    private String name;
    private String surname;
    private String middlename;
    private String telephone;

    public OwnerModel(int id, String name, String surname, String middlename, String telephone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middlename = middlename;
        this.telephone = telephone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OwnerModel that = (OwnerModel) o;
        return id == that.id &&
                name.equals(that.name) &&
                surname.equals(that.surname) &&
                middlename.equals(that.middlename) &&
                telephone.equals(that.telephone);
    }

    @Override
    public String toString() {
        return "OwnerModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", telephone='" + telephone + '\'' +
                '}';
    }
}



