package sportgrounds.repository;

import sportgrounds.SportException;
import sportgrounds.model.SportGroundModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class SportGroundOperations implements Repository<SportGroundModel> {

    private ConnectionSupplier connectionSupplier;
    public SportGroundOperations(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public SportGroundModel findByID(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl WHERE id = " + "'" + id + "'")) {

                SportGroundModel model = new SportGroundModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                model.setGround_id(resultSet.getLong(1));
                model.setCountry(resultSet.getString(2));
                model.setCity(resultSet.getString(3));
                model.setStreet(resultSet.getString(4));
                model.setHome(resultSet.getString(5));
                model.setWorktime(resultSet.getString(6));
                model.setProfile(resultSet.getLong(7));
                model.setOwner_id(resultSet.getLong(8));
                return model;
            } catch (SQLException e) {
                throw new SportException("Error in findByID CurrencyRepository", e);
            }
        } catch (SQLException e) {
            throw new SportException("Error in findByID CurrencyRepository", e);
        }
    }

    @Override
    public Collection<SportGroundModel> findAll() {
        try(Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM maininfo.grounds")) {

                ArrayList<SportGroundModel> models= new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    SportGroundModel model = new SportGroundModel();
                    model.setGround_id(resultSet.getLong(1));
                    model.setCountry(resultSet.getString(2));
                    model.setCity(resultSet.getString(3));
                    model.setStreet(resultSet.getString(4));
                    model.setHome(resultSet.getString(5));
                    model.setWorktime(resultSet.getString(6));
                    model.setProfile(resultSet.getLong(7));
                    model.setOwner_id(resultSet.getLong(8));
                    models.add(model);
                }
                return models;
            } catch (SQLException e) {
                throw new SportException("Error in findAll CurrencyRepository", e);
            }
        }catch (SQLException e){
            throw new SportException("Error in findAll CurrencyRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM maininfo.grounds WHERE id = " + id)){
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new SportException("Error in remove CurrencyRepository", e);
            }
        } catch (SQLException e){
            throw new SportException("Error in remove CurrencyRepository", e);
        }

    }

    @Override
    public SportGroundModel save(SportGroundModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO maininfo.grounds (country, city, street, home, worktime, profile, owner_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS)){

                preparedStatement.setString(1, model.getCountry());
                preparedStatement.setString(2, model.getCity());
                preparedStatement.setString(3, model.getStreet());
                preparedStatement.setString(4, model.getHome());
                preparedStatement.setString(5, model.getWorktime());
                preparedStatement.setLong(6, model.getProfile());
                preparedStatement.setLong(7, model.getOwner_id());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setGround_id(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new SportException("Error in save CurrencyRepository", e);
            }

        } catch (SQLException e) {
            throw new SportException("Error in save CurrencyRepository", e);
        }
    }

    @Override
    public void update(SportGroundModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE maininfo.grounds SET country = " + "'" + model.getCountry() + "'" +
                            ", city = " + "'" + model.getCity() + "'" +
                            ", street = " + "'" + model.getStreet() + "'" +
                            ", home = " + "'" + model.getHome() + "'" +
                            ", worktime = " + "'" + model.getWorktime() + "'" +
                            ", profile = " + "'" + model.getProfile() + "'" +
                            ", owner_id = " + "'" + model.getOwner_id() + "'" +
                            "WHERE ID = " + model.getGround_id()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new SportException("Error in update CurrencyRepository", e);
            }
        }catch (SQLException e){
            throw new SportException("Error in update CurrencyRepository", e);
        }
    }

}
