package sportgrounds.repository;

import sportgrounds.model.ProfileModel;

import java.util.Collection;

public class ProfileOperations implements Repository<ProfileModel> {

    private ConnectionSupplier connectionSupplier;
    public ProfileOperations(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    public ProfileModel findByID(long id) {
        return null;
    }

    public Collection<ProfileModel> findAll() {
        return null;
    }

    public void remove(long id) {

    }

    public ProfileModel save(ProfileModel model) {


        return null;
    }

    public void update(ProfileModel model) {

    }
}
