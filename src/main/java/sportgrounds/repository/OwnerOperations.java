package sportgrounds.repository;

import sportgrounds.model.OwnerModel;

import java.security.acl.Owner;
import java.util.Collection;

public class OwnerOperations implements Repository<OwnerModel> {

    private ConnectionSupplier connectionSupplier;
    public OwnerOperations(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    public OwnerModel findByID(long id) {
        return null;
    }

    public Collection<OwnerModel> findAll() {
        return null;
    }

    public void remove(long id) {

    }

    public OwnerModel save(OwnerModel model) {
        return null;
    }

    public void update(OwnerModel model) {

    }
}
