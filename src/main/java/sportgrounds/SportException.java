package sportgrounds;

public class SportException extends RuntimeException {
    public SportException(String message, Throwable exception){
        super(message,exception);
    }
}